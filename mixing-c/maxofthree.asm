;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: maxofthree.asm $
;; $Date: 2023-12-19 03:56:01 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)

;;; Output Functions (global)
global maxofthree

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]

;; `text' is my code, vector table plus constants.
[SECTION .text]
maxofthree:
    mov     rax, rdi                ; result (rax) initially holds x
    cmp     rax, rsi                ; is x less than y?
    cmovl   rax, rsi                ; if so, set result to y
    cmp     rax, rdx                ; is max(x,y) less than z?
    cmovl   rax, rdx                ; if so, set result to z
    ret                             ; the max will be in rax
