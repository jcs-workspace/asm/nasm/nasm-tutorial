;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: triangle.asm $
;; $Date: 2023-12-17 00:32:00 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)

;;; Output Functions (global)
global _start

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]
    maxlines equ 8
    dataSize equ 44

output: resb dataSize

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]

;; `text' is my code, vector table plus constants.
[SECTION .text]
_start:
    mov rdx, output             ; rdx holds address of next byte to write
    mov r8, 1                   ; initial line length
    mov r9, 0                   ; number of stars written on line so far

line:
    mov byte[rdx], "*"          ; write single star
    inc rdx                     ; advance pointer to next cell to write
    inc r9                      ; "count" number so far on line
    cmp r9, r8                  ; did we reach the number of stars for this line?
    jne line                    ; not yet, keep writing on this line

lineDone:
    mov byte[rdx], 10           ; write a new line char
    inc rdx                     ; and move pointer to where next char goes
    inc r8                      ; next line will be one char longer
    mov r9, 0                   ; reset count of stars written on this line
    cmp r8, maxlines            ; wait, did we already finish the last line?
    jng line                    ; if not, begin writing this line

done:
    mov rax, 1                  ; system call for write
    mov rdi, 1                  ; file handle 1 is stdout
    mov rsi, output             ; address of string to output
    mov rdx, dataSize           ; number of bytes
    syscall                     ; invoke operating system to do the write
    mov rax, 60                 ; system call for exit
    xor rdi, rdi                ; exit code 0
    syscall                     ; invoke operating system to exit
