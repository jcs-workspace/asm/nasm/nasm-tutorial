;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: factorial.asm $
;; $Date: 2023-12-21 00:48:05 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)

;;; Output Functions (global)
global factorial

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]

;; `text' is my code, vector table plus constants.
[SECTION .text]
factorial:
    cmp     rdi, 1                  ; n <= 1?
    jnbe    L1                      ; if not, go do a recursive call
    mov     rax, 1                  ; otherwise return 1
    ret

L1:
    push    rdi                     ; save n on stack (also aligns %rsp!)
    dec     rdi                     ; n-1
    call    factorial               ; factorial(n-1), result goes in %rax
    pop     rdi                     ; restore n
    imul    rax, rdi                ; n * factorial(n-1), stored in %rax
    ret
