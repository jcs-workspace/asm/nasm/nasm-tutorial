/**
 * $File: callfactorial.c $
 * $Date: 2023-12-21 01:30:09 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2023 by Shen, Jen-Chieh $
 */

#include <inttypes.h>
#include <stdio.h>

uint64_t factorial(uint64_t n);

int main() {
  for (uint64_t i = 0; i < 20; ++i) {
    printf("factorial(%2lu) = %lu\n", i, factorial(i));
  }
  return 0;
}
