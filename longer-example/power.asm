;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: power.asm $
;; $Date: 2023-12-20 02:13:57 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)
extern printf
extern puts
extern atoi

;;; Output Functions (global)
global main

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]
answer:
    db "%d", 10, 0
badArgumentCount:
    db "Requires exactly two arguments", 10, 0
negativeExponent:
    db "The exponent may not be negative", 10, 0

;; `text' is my code, vector table plus constants.
[SECTION .text]
main:
    push r12                    ; save callee-save registers
    push r13
    push r14
    ; By pushing 3 registers our stack is already aligned for calls

    cmp rdi, 3                  ; must have exactly two arguments
    jne error1

    mov r12, rsi               ; argv

;; We will be ecx to count down form the exponent to zero, esi to hold the
;; value of the base, and eax to hold the running product.

    mov rdi, [r12+16]           ; argv[2]
    call atoi                   ; y in eax
    cmp eax, 0                  ; disallow negative exponents
    jl error2
    mov r13d, eax               ; y in r13d

    mov rdi, [r12+8]            ; argv
    call atoi                   ; x in eax
    mov r14d, eax              ; x in r14d

    mov eax, 1                  ; start with answer = 1

check:
    test r13d, r13d             ; we're counting y down to 0
    jz gotit                    ; done
    imul eax, r14d              ; multiply in another x
    dec r13d
    jmp check

gotit:                          ; print report on success
    mov rdi, answer
    movsxd rsi, eax
    xor rax, rax
    call printf
    jmp done

error1:                         ; print error message
    mov edi, badArgumentCount
    call puts
    jmp done

error2:                         ; print error message
    mov edi, negativeExponent
    call puts

done:                           ; restore saved registers
    pop r14
    pop r13
    pop r12
    ret
