# nasm-tutorial

Follow the tutorial [NASM Tutorial](https://cs.lmu.edu/~ray/notes/nasmtutorial/).

## Note

Defining Data and Reserving Space

```asm
      db    0x55                ; just the byte 0x55
      db    0x55,0x56,0x57      ; three bytes in succession
      db    'a',0x55            ; character constants are OK
      db    'hello',13,10,'$'   ; so are string constants
      dw    0x1234              ; 0x34 0x12
      dw    'a'                 ; 0x61 0x00 (it's just a number)
      dw    'ab'                ; 0x61 0x62 (character constant)
      dw    'abc'               ; 0x61 0x62 0x63 0x00 (string)
      dd    0x12345678          ; 0x78 0x56 0x34 0x12
      dd    1.234567e20         ; floating-point constant
      dq    0x123456789abcdef0  ; eight byte constant
      dq    1.234567e20         ; double-precision float
      dt    1.234567e20         ; extended-precision float
```

## :link: Link

- [How to compile nasm program calling printf? [duplicate]](https://stackoverflow.com/questions/65912204/how-to-compile-nasm-program-calling-printf)
- [What are the differences comparing PIE, PIC code and executable on 64-bit x86 platform?](https://stackoverflow.com/questions/28119365/what-are-the-differences-comparing-pie-pic-code-and-executable-on-64-bit-x86-pl)
