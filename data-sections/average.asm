;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: average.asm $
;; $Date: 2023-12-21 00:17:54 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)

;;; Output Functions (global)
global main

extern atoi
extern printf

default rel

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]
count:  dq 0
sum:    dq 0
format: db "%g", 10, 0
error:  db "There are no command line arguments to average", 10, 0

;; `text' is my code, vector table plus constants.
[SECTION .text]
main:
    dec rdi                     ; argc-1, since we don't count program once
    jz nothingToAverage
    mov [count], rdi            ; save number of real arguments

accumulate:
    push rdi                    ; save register across call to atoi
    push rsi
    mov rdi, [rsi+rdi*8]        ; argv[rdi]
    call atoi                   ; now rax has the int value of arg
    pop rsi                     ; restore registers after atoi call
    pop rdi
    add [sum], rax              ; accumulate sum as we go
    dec rdi                     ; count down
    jnz accumulate              ; more arguments?

average:
    cvtsi2sd xmm0, [sum]
    cvtsi2sd xmm1, [count]
    divsd xmm0, xmm1            ; xmm0 is sum/count
    mov rdi, format             ; 1st arg to printf
    mov rax, 1                  ; printf is varargs, there is 1 non-int argument

    sub rsp, 8                  ; align stack pointer
    call printf                 ; printf(format, sum/count)
    add rsp, 8                  ; restore stack pointer

    ret

nothingToAverage:
    mov rdi, error
    xor rax, rax
    call printf
    ret
