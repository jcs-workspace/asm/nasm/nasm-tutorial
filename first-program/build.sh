#!/bin/bash
# ========================================================================
# $File: build.sh $
# $Date: 2023-12-16 22:51:23 $
# $Revision: $
# $Creator: Jen-Chieh Shen $
# $Notice: See LICENSE.txt for modification and distribution information
#                   Copyright © 2023 by Shen, Jen-Chieh $
# ========================================================================

nasm -felf64 hello.asm && ld hello.o && ./a.out
