;; -*- mode: nasm; lexical-binding: t -*-
;; ----------------------------------------------------------------------------------------
;; Writes "Hello, World" to the console using only system calls. Runs on 64-bit Linux only.
;; To assemble and run:
;;
;;     nasm -felf64 hello.asm && ld hello.o && ./a.out
;; ----------------------------------------------------------------------------------------

;; Input Functions (extern)

;;; Output Functions (global)
global _start


;; `bss' is for the uninitialized data in RAM which
;;  is initialized with zero in the startup code.
[SECTION .bss]


;; `data' is for initialized variables, and it counts
;; for RAM and FLASH. The linker allocates the data
;;  in FLASH which then is copied from ROM to RAM in
;;  the startup code.
[SECTION .data]
message: db "Hello, World", 10  ; note the newline at the end

;; `text' is my code, vector table plus constants.
[SECTION .text]
_start:
    mov rax, 1                  ; system call for write
    mov rdi, 1                  ; file handle 1 is stdout
    mov rsi, message            ; address of string to output
    mov rdx, 13                ; number of bytes
    syscall                     ; invoke operating system to do the write
    mov rax, 60                 ; system call for exit
    xor rdi, rdi                ; exit code 0
    syscall                     ; invoke operating system to exit
