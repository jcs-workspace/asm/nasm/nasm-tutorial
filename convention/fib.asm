;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: fib.asm $
;; $Date: 2023-12-19 03:07:56 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)
extern  printf

;;; Output Functions (global)
global  main

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]


;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]
format: db "%20ld", 10, 0

;; `text' is my code, vector table plus constants.
[SECTION .text]
main:
    push rbx                    ; we have to save this since we use it

    mov ecx, 90                 ; ecx will countdown to 0
    xor rax, rax                ; rax will hold the current number
    xor rbx, rbx                ; rbx will hold the next number
    inc rbx                     ; rbx is originally 1

print:
    ; We need to call printf, but we are using rax, rbx, and rcx.  printf
    ; may destroy rax and rcx so we will save these before the call and
    ; restore them afterwards.
    push rax                    ; caller-save register
    push rcx                    ; caller-save register

    mov rdi, format             ; set 1st parameter (format)
    mov rsi, rax                ; set 2nd parameter (current_number)
    xor rax, rax                ; because printf is varargs

    call printf                 ; printf(format, current_number)

    pop rcx                     ; restore caller-save register
    pop rax                     ; restore caller-save register

    mov rdx, rax                ; save the current number
    mov rax, rbx                ; next number is now current
    add rbx, rdx                ; get the new next number
    dec ecx                     ; count down
    jnz print                   ; if not done counting, do some more

    pop rbx                     ; restore rbx before returning
    ret

