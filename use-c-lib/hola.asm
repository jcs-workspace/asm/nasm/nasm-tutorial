;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: hola.asm $
;; $Date: 2023-12-18 02:07:57 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)
extern puts

;;; Output Functions (global)
global main

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]
message:
    db "Hola, mundo", 0         ; Note strings must be terminated with 0 in C

;; `text' is my code, vector table plus constants.
[SECTION .text]
main:                           ; This is called by the C library startup code
    push rbx                    ; Call stack must be aligned
    mov rdi, message            ; First integer (or pointer) argument in rdi
    call puts                   ; puts(message)
    pop rbx                     ; Fix up stack before returning
    ret
