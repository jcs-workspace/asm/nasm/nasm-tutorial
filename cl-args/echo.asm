;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: echo.asm $
;; $Date: 2023-12-19 23:33:18 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)
extern puts

;;; Output Functions (global)
global main

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]

;; `text' is my code, vector table plus constants.
[SECTION .text]
main:
    push rdi                    ; save registers that puts uses
    push rsi
    sub rsp, 8                  ; must align stack before call

    mov rdi, [rsi]              ; the argument string to display
    call puts                   ; print it

    add rsp, 8                  ; restore %rsp to pre-aligned value
    pop rsi                     ; restore registers puts used
    pop rdi

    add rsi, 8                  ; point to next argument
    dec rdi                     ; count down
    jnz main                    ; if not done counting keep going

    ret
