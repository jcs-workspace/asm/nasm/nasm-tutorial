;; -*- mode: nasm; lexical-binding: t -*-
;; ========================================================================
;; $File: sum.asm $
;; $Date: 2023-12-20 23:36:05 $
;; $Revision: $
;; $Creator: Jen-Chieh Shen $
;; $Notice: See LICENSE.txt for modification and distribution information
;;                   Copyright © 2023 by Shen, Jen-Chieh $
;; ========================================================================

;; Input Functions (extern)

;;; Output Functions (global)
global sum

;; `bss' is for the uninitialized data in RAM which is initialized with zero
;; in the startup code.
[SECTION .bss]

;; `data' is for initialized variables, and it counts for RAM and FLASH.
;; The linker allocates the data in FLASH which then is copied from ROM to
;; RAM in the startup code.
[SECTION .data]

;; `text' is my code, vector table plus constants.
[SECTION .text]
sum:
    xorpd xmm0, xmm0            ; initialize the sum to 0
    cmp rsi, 0                  ; special case for length = 0
    je done

next:
    addsd xmm0, [rdi]           ; add in the current array element
    add rdi, 8                  ; move to next array element
    dec rsi                     ; count down
    jnz next                    ; if not done counting, continue

done:
    ret                         ; return value already in xmm0
